"use strict"

const platform = chrome
var monCount = null
var sharCount = null
var totalSecCount = null
var secCount = 0


// TODO: Move to platform
// List of pages on which extension is forbidden for security considerations.
// key is the URL prefix and value is the message to the user.
const restrictedPrefixes = {
	"about:": "Firefox internal page.",
	"https://addons.mozilla.org/": "Mozilla extension store (AMO) page.",
	"https://testpilot.firefox.com/": "Firefox Test Pilot page."
}

/**
 * Check if page is restricted for security reasons and display appropriate content.
 * @returns {Boolean} true if page is restricted, false otherwise.
 */
function pageNothingToDo(url){
	// Check the current page against all restricted pages
	for (const prefix in restrictedPrefixes)
		// this is equivalent to startsWith()
		if (url.substring(0,prefix.length) === prefix){
			// Match found, so we remove all regular content and display explanation
			const text = "This is a safe " + restrictedPrefixes[prefix] + "\nThe page is: " + url
			// Remove all content
			document.body.removeChild(document.getElementById("allContent"))
			// Show explanation and message
			var message = document.createElement("P")
			message.innerText = text
			document.body.appendChild(message)
			return true
		}

	// If page does not match any of the restricted page prefixes, remove message
	document.body.removeChild(document.getElementById("safeMessage"))
	return false
}

/* TODO: Refactor to avoid changing classes of so many elements */
function toggleMainContent(canClose){
	const allContent = document.querySelector("#allContent")
	const mainMenu = document.querySelector("#main-menu")

	if (allContent.classList.contains("all-content-half")){
		allContent.classList.remove("all-content-half")
		allContent.classList.add("all-content-full")
		mainMenu.classList.remove("main-menu-full")
		mainMenu.classList.add("main-menu-half")
		document.querySelector("#details").classList.remove("details-hide")
	}
	else
	if(canClose){
		allContent.classList.remove("all-content-full")
		allContent.classList.add("all-content-half")
		mainMenu.classList.remove("main-menu-half")
		mainMenu.classList.add("main-menu-full")
		document.querySelector("#details").classList.add("details-hide")
	}
	clearHighlighted()
}

/* TODO: Refactor to avoid reloading the iframe, if it is already displayed */
function loadDetails(detailsURL, tabId){
	toggleMainContent()
	const iframe = document.getElementById("details")
	iframe.setAttribute("src", detailsURL+"?tabId="+tabId)
}

function clearHighlighted(){
	$("#monTitle").removeClass("highlight-title");
	$("#sharTitle").removeClass("highlight-title");
	$("#secTitle").removeClass("highlight-title");
	$("#debugTitle").removeClass("highlight-title");
}

document.addEventListener("DOMContentLoaded", function() {
	// Fix information tooltip?
	$(".info.link.icon").popup({ inline: true })

	/* Check if page is restricted and either display message or show regular UI. */
	platform.tabs.query({active: true, currentWindow: true }, function(activeTabs){

		// If page is restricted, just display a message and finish
		if (pageNothingToDo(activeTabs[0].url))
			return

		const tabId = activeTabs[0].id

        function initMain(data){
            console.log("Retrieved data from cookie storage")

            platform.tabs.getSelected(null, function(tab){
                var url = new URL(tab.url)
                console.log("Main url:"+url.origin)
                platform.cookies.getAll({url:url.origin},initMonitoring)
            });

			var sharingCount = 0
            for (var domain in data.domains){
            	totalSecCount++
                if (data.domains[domain].thirdparty === true){
					sharingCount++
                }
            }
			sharCount = sharingCount
			setSharing(sharCount)
			setSecurity(secCount)

		}
        const message = {recepient: "cookiestore.js", request: {reason: "data", section: "own"}, tabId: tabId}

		// Notify other pages (background) that popup is open
		platform.runtime.sendMessage(message,initMain)
		console.log("popup.js: Sending request to cookiestore")

		/* When Main Menu item is clicked, load the corresponding details iframe */
		document.getElementById("monitoring").addEventListener("click", function() {
			if($("#monTitle").hasClass("highlight-title")){
				toggleMainContent(true)
			}else{
				loadDetails("monitoring/monitoring.html", tabId)
				$("#monTitle").addClass("highlight-title");
			}
		})

		document.getElementById("sharing").addEventListener("click", function() {
			if($("#sharTitle").hasClass("highlight-title")){
				toggleMainContent(true)
			}
			else{
				loadDetails("sharing/sharing.html", tabId)
				$("#sharTitle").addClass("highlight-title")
			}
		})

		document.getElementById("security").addEventListener("click", function() {
			if($("#secTitle").hasClass("highlight-title")){
				toggleMainContent(true)
			}
			else{
				loadDetails("security/security.html", tabId)
				$("#secTitle").addClass("highlight-title");
			}
		})

		document.getElementById("debugging").addEventListener("click", function() {
			if($("#debugTitle").hasClass("highlight-title")){
				toggleMainContent(true)
			}
			else{
				loadDetails("debugging/debugging.html", tabId)
				$("#debugTitle").addClass("highlight-title");
			}
		})
	})
})

platform.runtime.onMessage.addListener(function(message, sender, sendResponse){
    if (platform.runtime.id !== sender.id){
        console.warn("Some other extension sent a messsage. We ignored it.", message, sender)
        return
    }

    if (message.recipient !== "popup.js") return
    console.log("Message received inside popup.js with type: " + message.type)

    if(message.type == "monInc") setMonitoring(++monCount)
    else if(message.type == "monDec") setMonitoring(--monCount)
    else if(message.type == "sharInc") setSharing(++sharCount)
	else if(message.type == "sharDec") setSharing(--sharCount)
	else if(message.type == "sharReset") {sharCount = 0; setSharing(sharCount)}
	else if(message.type == "secInc") setSecurity(++secCount)
	else if(message.type == "secDec") setSecurity(--secCount)
})

/* dynamically sharinglvl is changed, function is called */
/* class to store the lvl */
/*
var o={};
var Sharinglvl;
var rotateClock = 50;

Object.defineProperty(o, 'lvl', {
	get(){return Sharinglvl;},
	set(sharinglvl){sharingimage(sharinglvl);}
});

Object.defineProperty(o, 'rotatedegree', {
	get(){return rotateClock;},
	set(deg){sharingimage(deg);}
});
*/

var rangeShow = document.querySelector("#show");
var rangeClock =  document.querySelector('.meter-clock');
var Sharingimage = document.getElementById("sharingimage");
var Securityimage = document.getElementById("securityimage");
var SecurityText = document.getElementById("SecurityText");
var SharingText = document.getElementById("SharingText");
var MonitoringText = document.getElementById("MonitoringText");

function initMonitoring(mainCookies){
    console.log("Retrieved cookies for main domain and trying to set monitoring")
    console.log("Monitoring cookies count: " + mainCookies.length)
	var count = mainCookies.length
    if(count > 40) count = 40
	monCount = count
	setMonitoring(monCount)
}

function setMonitoring(count){
    MonitoringTextChange(count)
    var range = (count / 40) * 100
    rangeChange(range)
}

function setSharing(sharingCount){
    sharingTextChange(sharingCount)
    sharingchange(sharingCount)
}

function setSecurity(secCount){
	var percentage = Math.round((secCount / totalSecCount) * 100)
	SecurityTextChange(percentage + "%")
	if(percentage == 100){
		securitychange(2)
	}
	else{
		securitychange(4)
	}
}

/*Function that dynamically change the logo of sharing*/
function sharingchange(lvl){
	var image= document.getElementById('sharingimage');
	if(lvl < 5){
		image.src ='/img-t/sharing1.png';
	}else if(lvl < 12){
		image.src ='/img-t/sharing2.png';
	}else if(lvl > 12){
		image.src ='/img-t/sharing3.png';
	}else{
		console.log("Invalid Sharing Lvl Input.")
	}
}

/*Function that dynamically change the logo of security*/
function securitychange(lvl){
	var image= document.getElementById('securityimage');
	if(lvl == 1){
		image.src ='/img-t/security1.png';
	}else if(lvl == 2){
		image.src ='/img-t/security2alt.png';
	}else if(lvl == 3){
		image.src ='/img-t/security3.png';
	}else if(lvl == 4){
		image.src ='/img-t/security4.png';
	}else{
		console.log("invalid Security Lvl Input.");
	}
}

/*Function that dynamically change the Text*/

function SecurityTextChange(string){
	SecurityText.innerHTML = string;
}

function sharingTextChange(string){
	SharingText.innerHTML = string;
}

function MonitoringTextChange(string){
	MonitoringText.innerHTML = string;
}

/*Function that rotate the speedometer needle*/
function rangeChange(deg) {
	rangeClock.style.transform = 'rotate(' + (-90 + ((deg * 180) / 100)) + 'deg)';
}
