const platform = chrome

$(".ui.accordion").accordion({
    selector: {
        trigger: ".acc-title"
    }
})

$(".ui.dropdown").dropdown()

function initializeEye(){
    $(".eye").click(function() {
        var message = {recipient: "popup.js", type : "monInc"}
        if($(this).hasClass("slash")){
            platform.runtime.sendMessage(message)
        }else{
            message.type = "monDec"
            platform.runtime.sendMessage(message)
        }

        $(this).toggleClass("eye icon eye slash icon");
    });
}

$("#optOut").click(function() {
    $('.ui.modal').modal({
        // blurring: true
    }).modal('show');
    $(".ui.modal").modal({observeChanges: true});
    $('.ui.modal').modal('refresh');
    $('.ui.modal').modal('refresh');
});


document.addEventListener("DOMContentLoaded", function() {
    // Remember reference list item
    const referenceListItem = document.getElementById("reference-list-item")
    referenceListItem.parentNode.removeChild(referenceListItem)

    // Remember reference to the list
    const listNode = document.getElementById("list")

    function displayInformation(cookies){
        console.log("Retrieved cookies")
        displayList(cookies, referenceListItem, listNode)
    }

    platform.tabs.getSelected(null, function(tab){
        var url = new URL(tab.url)
        platform.cookies.getAll({url:url.origin},displayInformation)
    });
})

function displayList(cookies, referenceListItem, listNode){
    console.log("Going over the domains")
	if(cookies.length == 0){
		$("#monTitle").addClass("hide");
        $("#noMonitoring").removeClass("hide");
		$("#optOut").addClass("hide");
	}

    for (var cookie in cookies){
        const itemNode = createListElement(referenceListItem, cookies[cookie])
        listNode.appendChild(itemNode)
    }
    initializeEye()
}


function createListElement(referenceListItem, cookie){
    var node = referenceListItem.cloneNode(true)
// Set a unique ID
    node.id = cookie.name

// Display content
    node.querySelector(".domain-title").innerText = cookie.name

    const referenceContentNode = node.querySelector(".content")
	referenceContentNode.innerText = cookie.value
    return node
}