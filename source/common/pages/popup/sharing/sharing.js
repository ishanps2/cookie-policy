const platform = chrome
var information = null
var selectedCategory = "All"
var selectedState = "All"

$(".ui.accordion").accordion({
	selector: {
		trigger: ".acc-title"
	}
})

$("#select-category").dropdown({
	onChange: function(value, text, selectedItem) {
		console.log("Category dropdown changed and value is: " + value)
		selectedCategory = value
		filterList()
	}
})

$("#select-state").dropdown({
	onChange: function(value, text, selectedItem){
		selectedState = value

		if(value == "Allowed") selectedState = true
		else if (value == "Blocked") selectedState = false

		console.log("State dropdown changed and value is: " + value)
		filterList()
	}
})

function initializeCheckbox(){
	$(".ui.checkbox").checkbox({
		onChecked: function () {
            platform.runtime.sendMessage({recipient: "popup.js", type: "sharInc"})
            var domain = this.parentNode.parentNode.parentNode.getAttribute("domain")
            information.domains[domain].isShared = true
        },
        onUnchecked: function () {
            platform.runtime.sendMessage({recipient: "popup.js", type: "sharDec"})
            var domain = this.parentNode.parentNode.parentNode.getAttribute("domain")
            information.domains[domain].isShared = false
        }
	}) //Initialize after dynamic DOM addition
}


$("#disableAll").click(function(){
	$('.ui.checkbox').checkbox('uncheck');
    platform.runtime.sendMessage({recipient: "popup.js", type: "sharReset"})
})
//$('.ui.checkbox').checkbox('attach events', '#disableAll', 'check');


document.addEventListener("DOMContentLoaded", function() {
	const search = document.location.search
	const tabId = parseInt(search.substring("?tabId=".length))

	// Remember reference list item
	const referenceListItem = document.getElementById("reference-list-item")
	referenceListItem.parentNode.removeChild(referenceListItem)

	// Remember reference to the list
	const listNode = document.getElementById("list")

	const selectCategoryNode = document.getElementById("select-category")

	const message = {recepient: "cookiestore.js", request: {reason: "data", section: "thirdparty"}, tabId: tabId}

	function displayInformation(details){
		console.log("Retrieved domain information", details)
		information = details

		displayList(referenceListItem, listNode)
		displayCategoryFilters(information.domainCategories, selectCategoryNode)
	}

	platform.runtime.sendMessage(message, displayInformation)
})

function displayList(referenceListItem, listNode){
	var count = 0
	for (domain in information.domains){
		if (information.domains[domain].thirdparty === true){
			information.domains[domain].isShared = true
		    retrieveCookies(domain, information.domains[domain], referenceListItem, listNode)
            count++
        }
	}

	if(count == 0){
		$(".row").addClass("hide")
		$("#noSharing").removeClass("hide");
		$("#disableAll").addClass("hide")
	}
}

function retrieveCookies(domain, details, referenceListItem, listNode){
	platform.cookies.getAll({domain:domain},function(cookies){
		console.log("Retrieved cookies for domain:"+domain+". Count:"+cookies.length)
		const itemNode = createListElement(referenceListItem, domain, details, cookies)
		listNode.appendChild(itemNode)
		initializeCheckbox()

	})
}

function createListElement(referenceListItem, domain, details, contents){
	var node = referenceListItem.cloneNode(true)
	// Delete duplicate id
	node.removeAttribute("id")
	node.setAttribute("domain", domain)

// Display content
	node.querySelector(".domain-title").innerText = domain
	/*if(domain.isShared && !domain.isShared){
		node.querySelector("acc-toggle").classList.remove("checked")
	}*/

	const referenceContentNode = node.querySelector(".reference-list-item-content")
	const contentParent = referenceContentNode.parentNode
	referenceContentNode.parentNode.removeChild(referenceContentNode)
	if (contents.length === 0){
		var contentNode = referenceContentNode.cloneNode(true)
		contentNode.innerText = "No information is saved right now, however some information might be collected and shared in the future."
		contentParent.appendChild(contentNode)
	}
	for (content of contents){
		var contentNode = referenceContentNode.cloneNode(true)
		contentNode.innerText = "Name: " + content.name + "\nValue: " + content.value
		contentParent.appendChild(contentNode)
	}
	return node
}

function displayCategoryFilters(domainCategories, selectCategoryNode){
	domainCategories.unshift("All")
	for (domainCategory of domainCategories){
		var option = document.createElement("option")
		option.text = domainCategory
		selectCategoryNode.add(option)
	}
}

function filterList(type, value){
	$(".acc-list-item").each(function(){
		const domain = this.getAttribute("domain")
		const info = information.domains[domain]

		const sameCategory = selectedCategory === "All" || info.category === undefined || (info.category && selectedCategory === info.category.category)
		const realState = info.isShared
		const sameState = selectedState === "All" || realState === selectedState
		if(sameCategory && sameState)
			this.classList.remove("hide")
		else
			this.classList.add("hide")
	})
}
