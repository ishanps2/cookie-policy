const platform = chrome
var domains = null
var selectedSecurity = "Any"
var selectedthreat = "Any"


$(".ui.accordion").accordion({
    selector: {
        trigger: ".acc-title"
    }
})

$("#select-security").dropdown({
    onChange: function(value, text, selectedItem) {
        console.log("Category dropdown changed and value is: " + value)
        selectedSecurity = value
        filterList()
    }
})

$("#select-threat").dropdown({
    onChange: function(value, text, selectedItem) {
        console.log("Category dropdown changed and value is: " + value)
        selectedthreat = value
        filterList()
    }
})

function filterList(type, value){
    $(".acc-list-item").each(function(){
        const domain = this.getAttribute("domain")
        const info = information.domains[domain]
        var currStateSecurity = selectedSecurity
        var currStateThreat = selectedthreat
        var security

        if(info.isSecure == false)
            security = "Insecure"
        else {
            security = "Secure"
        }

        if(currStateSecurity == "Any") currStateSecurity = security
        if(currStateThreat == "Any") currStateThreat = info.threat


        if(currStateSecurity == security && currStateThreat == info.threat)
            $(this).removeClass("hide")
        else {
            $(this).addClass("hide")
        }

    })
}

function initializeLock(node){
    var unlock = node.querySelector(".securitylvl")

    unlock.addEventListener("click", function() {
        var message = {recipient: "popup.js", type:"secInc"}
        var domain = this.parentNode.parentNode.parentNode.getAttribute("domain")

        if($(this).hasClass("lock")){
            message.type = "secDec"
            platform.runtime.sendMessage(message)
            information.domains[domain].isSecure = false;
        }else{
            platform.runtime.sendMessage(message)
            information.domains[domain].isSecure = true;
        }

        $(this).toggleClass("unlock icon lock icon");


    });
}


document.addEventListener("DOMContentLoaded", function() {
    const search = document.location.search
    const tabId = parseInt(search.substring("?tabId=".length))

    // Remember reference list item
    const referenceListItem = document.getElementById("reference-list-item")
    referenceListItem.parentNode.removeChild(referenceListItem)

    // Remember reference to the list
    const listNode = document.getElementById("list")

    const message = {recepient: "cookiestore.js", request: {reason: "data", section: "security"}, tabId: tabId}


    function displayInformation(details){
        console.log("Retrieved domain information", details)
        information = details

        displayList(referenceListItem, listNode)
    }

    platform.runtime.sendMessage(message, displayInformation)
})

function displayList(referenceListItem, listNode){
    var count = 0
    for (domain in information.domains){
        information.domains[domain].isSecure = false;
        retrieveCookies(domain, information.domains[domain], referenceListItem, listNode)
        count++
    }
    if(count == 0){
        $(".row").addClass("hide")
        $("#safe").removeClass("hide");
    }
}

function retrieveCookies(domain, details, referenceListItem, listNode){
    platform.cookies.getAll({domain:domain},function(cookies){
        console.log("Retrieved cookies for domain:"+domain+". Count:"+cookies.length)
        const itemNode = createListElement(referenceListItem, domain, details, cookies)
        listNode.appendChild(itemNode)
        initializeLock(itemNode)
    })
}

function createListElement(referenceListItem, domain, details, contents){
    var node = referenceListItem.cloneNode(true)
    // Delete duplicate id
    node.removeAttribute("id")
    node.setAttribute("domain", domain)

// Display content
    node.querySelector(".domain-title").innerText = domain
    /*if(domain.isShared && !domain.isShared){
        node.querySelector("acc-toggle").classList.remove("checked")
    }*/

    const referenceContentNode = node.querySelector(".reference-list-item-content")
    const contentParent = referenceContentNode.parentNode
    referenceContentNode.parentNode.removeChild(referenceContentNode)
    if (contents.length === 0){
        var contentNode = referenceContentNode.cloneNode(true)
        contentNode.innerText = "No information is saved right now, however some information might be collected and shared in the future."
        contentParent.appendChild(contentNode)
    }
    for (content of contents){
        var contentNode = referenceContentNode.cloneNode(true)
        contentNode.innerText = "Name: " + content.name + "\nValue: " + content.value
        contentParent.appendChild(contentNode)
    }
    return node
}
