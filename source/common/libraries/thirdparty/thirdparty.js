/**
 * Determine if resource is a third-party based only on domain names
 * @param   origin - string, domain of the orgin page or frame
 * @param   resource - string, domain of the resource
 * @returns boolean - true if thirdpary, false otherwise
 */

// Import Public Suffix List
const psl = require("psl")

// Import Disconnect blacklist
const blacklist = require("../../includes/disconnect/services.json").categories

//const entitylist = require("../../includes/disconnect/disconnect-entitylist.json")

const categories = Object.keys(blacklist)

const lookup = blacklist //initializeBlacklistStructure(blacklist)

/**
 * The psl.parse(...) returns object with input, tld, sld, domain, subdomain, listed
 */

function thirdparty (origin, resource){
	// TODO: add caching to avoid repeatedly parsing the same inputs
	// TODO: check entities list
	// This checks that resource and origin have the same private topmost domain.
	const originDomain = psl.parse(origin).domain
	const resourceDomain = psl.parse(resource).domain

	return originDomain !== resourceDomain
}

function categorize(resource){
	for (const category in blacklist)
		for (const record of blacklist[category]){
			// For some reason, records are dictionaries most having a single entry
			// Only one record has multiple keys
			// "ItIsATracker": { 'https://itisatracker.com/': [ 'itisatracker.com' ], dnt: 'eff' }
			// this is a test domain 
			// TODO: figure out what it isTreat it as dictionary anyway
			for (const label in record){
				// label can be displayed in the UI
				// For some reason, record[label] are dictionaries each having a single entry
				if (Object.keys(record[label]).length !== 1){
					console.warn("Omitting weird record in blacklist:", "category=" + category, label, record[label])
					continue
				}
				const url = Object.keys(record[label])[0]
				const domains = record[label][url]

				for (domain of domains){
					if (resource.endsWith(domain)){
						return {category:category, label:label, ownerUrl:url, record: record[label]}
					}
				}
			}
		}
	return undefined
}

module.exports = {
	thirdparty: thirdparty,
	categories: categories,
	categorize: categorize
}
