const path = require("path")

const ncp = require("ncp").ncp
ncp.limit = 16

const fs = require("fs")

const base = "./source/common/"
const build = "./build/"
const node_modules = "./node_modules/"

const dirs = ["", "pages/", "img-t/", "includes/", "includes/semantic-ui-css", "includes/jquery"]

const copy = [
	"pages/",
	"img-t/",
	"manifest.json"
]

const files = [
	"background/cookiestore.js",
	"content_scripts/inject.js",
	"web_accessible_resources/inject.js"
]

const node_modules_dependencies = {
	"semantic-ui-css/semantic.min.js": "semantic-ui-css/semantic.min.js",
	"semantic-ui-css/semantic.min.css": "semantic-ui-css/semantic.min.css",
	"semantic-ui-css/themes/": "semantic-ui-css/themes/",
	"jquery/dist/jquery.min.js": "jquery/jquery.min.js"
}

// Create all dirs if they do not exists yet
for (const dir of dirs)
	if (!fs.existsSync(build+dir))
		fs.mkdirSync(build+dir)

// Copy all files that do not require processing
for (const src of copy)
	ncp(base + src, build + src, function (err) {
		if (err)
			return console.error(err)
		console.log("Copied " + src)
	})

//Copy all the dependencies that don't require processing
for (const dependency in node_modules_dependencies) {
	const source = node_modules + dependency
	const destination = build + "includes/" + node_modules_dependencies[dependency]
	ncp(source, destination, function (err) {
		if (err)
			return console.error(err)
		console.log("Copied dependency " + dependency + " from " + source + " into " + destination)
	})
}

// Add all files that do require processing to webpack work order
var entries = {}
for (const file of files){
	entries[file] = base + file
	console.log("Added for packaging " + file)
}

// Ask webpack to process these files
module.exports = {
	entry: entries,
	output: {
		path: path.resolve(__dirname, build),
		filename: "[name]"
	}
}
